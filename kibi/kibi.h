/*
 * Copyright (c) 2010-2011, Benjamin Drung <benjamin.drung@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef __KIBI_H__
#define __KIBI_H__

typedef unsigned long long int kibi_filesize_t;

/**
 * Divides a size by the factor indicated by the given level.
 * The given size is converted into the same unit that is returned by
 * kibi_get_unit_for_size(level) and kibi_get_unit_for_transfer_rate(level).
 *
 * This function can be used for letting the user change the size into the unit
 * from the given level.
 *
 * @param size a file size in bytes.
 * @param level an integer referring to a prefix.
 * @returns the size in the unit from the given level.
 */
double kibi_divide_size(kibi_filesize_t size, unsigned int level);

/**
 * Formats a computer memory size into a human readable string.
 * Sizes are rounded to the nearest size prefix with at least three
 * significant digits. The prefix units base is always 1024, because
 * computer memory size is often a multiple of two.
 *
 * Use this function if you want to display the size of your RAM. Use
 * kibi_format_size() instead if you want to display how much RAM a
 * files uses or how much RAM is free.
 *
 * This string should be freed with free() when not needed any longer.
 *
 * @param memory_size a computer memory size in bytes.
 * @returns a newly-allocated formatted string containing a human readable
 *          computer memory file size.
 */
char *kibi_format_memory_size(kibi_filesize_t memory_size);

/**
 * Formats a SI value into a human readable string.
 * Values are rounded to the nearest SI prefix with at least three
 * significant digits. The suffix is appended to the string and should
 * contain the unit.
 *
 * Use this function for SI or SI derived units, for example for hertz.
 * Do not use it for bytes!
 *
 * This string should be freed with free() when not needed any longer.
 *
 * @param value a value that should be formatted.
 * @param unit a string containing the unit.
 * @returns a newly-allocated formatted string containing a human readable
 *          value.
 */
char *kibi_format_si_unit(kibi_filesize_t value, const char *unit);

/**
 * Formats a size into a human readable string.
 * Sizes are rounded to the nearest size prefix with at least three
 * significant digits. The prefix units base is 1000 or 1024 depending on
 * the user's configuration.
 *
 * This string should be freed with free() when not needed any longer.
 *
 * @param size a file size in bytes.
 * @returns a newly-allocated formatted string containing a human readable
 *          file size.
 */
char *kibi_format_size(kibi_filesize_t size);

/**
 * Formats a size into a human readable string.
 * Sizes are rounded to the nearest size prefix with at least three
 * significant digits. The size is displayed twice. Once with prefix
 * units base 1000 and once with 1024.
 *
 * This string should be freed with free() when not needed any longer.
 *
 * @param size a file size in bytes.
 * @returns a newly-allocated formatted string containing a human readable
 *          file size.
 */
char *kibi_format_size_detailed(kibi_filesize_t size);

/**
 * Formats a transfer rate into a human readable string.
 * Sizes are rounded to the nearest size prefix with at least three
 * significant digits. The prefix units base is 1000 or 1024 depending on
 * the user's configuration.
 *
 * This string should be freed with free() when not needed any longer.
 *
 * @param transfer_rate a transfer rate in bytes/s.
 * @returns a newly-allocated formatted string containing a human readable
 *          transfer rate.
 */
char *kibi_format_transfer_rate(kibi_filesize_t transfer_rate);

/**
 * Formats a value into a human readable string.
 * Sizes are rounded to the nearest size prefix with at least three
 * significant digits. The prefix units base is 1000 or 1024 depending on
 * the user's configuration. The suffix is appended to the string and should
 * contain the unit.
 *
 * Use this function with care! kibi_format_size() should be used for file
 * sizes instead. kibi_format_transfer_rate() should be used for transfer
 * rates instead.
 *
 * This string should be freed with free() when not needed any longer.
 *
 * @param value a value that should be formatted.
 * @param suffix a string containing the unit.
 * @returns a newly-allocated formatted string containing a human readable
 *          value.
 */
char *kibi_format_value(kibi_filesize_t value, const char *suffix);

/**
 * Gets the byte unit for a given file size level.
 *
 * Depending on the configuration, the function returns "B" for level 0,
 * "kB"/"KiB"/"KB" for level 1, "MB"/"MiB" for level 2, and so on.
 *
 * @param level an integer referring to a prefix.
 * @returns a byte unit string for the given level.
 */
const char *kibi_get_unit_for_size(unsigned int level);

/**
 * Gets the transfer rate unit for a given transfer rate level.
 *
 * Depending on the configuration, the function returns "B/s" for level 0,
 * "kB/s"/"KiB/s"/"KB/s" for level 1, "MB/s"/"MiB/s" for level 2, and so on.
 *
 * @param level an integer referring to a prefix.
 * @returns a transfer rate unit string for the given level.
 */
const char *kibi_get_unit_for_transfer_rate(unsigned int level);

/**
 * Multiplies a size with the factor indicated by the given level.
 * The given size is converted from the same unit that is returned by
 * kibi_get_unit_for_size(level) and kibi_get_unit_for_transfer_rate(level)
 * into bytes.
 *
 * This function can be used for converting a user input size into bytes.
 *
 * @param size a file size in the unit from the given level.
 * @param level an integer referring to a prefix.
 * @returns the size in bytes.
 */
kibi_filesize_t kibi_multiply_size(double size, unsigned int level);

/**
 * Formats a computer memory size into a human readable string.
 * Sizes are rounded to the nearest size prefix with at least three
 * significant digits. The prefix units base is always 1024, because
 * computer memory size is often a multiple of two.
 *
 * Use this function if you want to display the size of your RAM. Use
 * kibi_n_format_size() instead if you want to display how much RAM a
 * files uses or how much RAM is free.
 *
 * @param dest The formatted string is written to dest.
 * @param n The function write at most n bytes (including the trailing '\0')
 *          to dest.
 * @param memory_size a computer memory size in bytes.
 * @returns the number of characters printed (not including the trailing '\0').
 *          If the output is truncated, the number of characters that would
 *          have been written to the string will be returned. A negative value
 *          is returned if an output error is encountered.
 */
int kibi_n_format_memory_size(char *dest, size_t n,
                              kibi_filesize_t memory_size);

/**
 * Formats a SI value into a human readable string.
 * Values are rounded to the nearest SI prefix with at least three
 * significant digits. The suffix is appended to the string and should
 * contain the unit.
 *
 * Use this function for SI or SI derived units, for example for hertz.
 * Do not use it for bytes!
 *
 * @param dest The formatted string is written to dest.
 * @param n The function write at most n bytes (including the trailing '\0')
 *          to dest.
 * @param value a value that should be formatted.
 * @param unit a string containing the unit.
 * @returns the number of characters printed (not including the trailing '\0').
 *          If the output is truncated, the number of characters that would
 *          have been written to the string will be returned. A negative value
 *          is returned if an output error is encountered.
 */
int kibi_n_format_si_unit(char *dest, size_t n, kibi_filesize_t value,
                          const char *unit);

/**
 * Formats a size into a human readable string.
 * Sizes are rounded to the nearest size prefix with at least three
 * significant digits. The prefix units base is 1000 or 1024 depending on
 * the user's configuration.
 *
 * @param dest The formatted string is written to dest.
 * @param n The function write at most n bytes (including the trailing '\0')
 *          to dest.
 * @param size a file size in bytes.
 * @returns the number of characters printed (not including the trailing '\0').
 *          If the output is truncated, the number of characters that would
 *          have been written to the string will be returned. A negative value
 *          is returned if an output error is encountered.
 */
int kibi_n_format_size(char *dest, size_t n, kibi_filesize_t size);

/**
 * Formats a size into a human readable string.
 * Sizes are rounded to the nearest size prefix with at least three
 * significant digits. The size is displayed twice. Once with prefix
 * units base 1000 and once with 1024.
 *
 * @param dest The formatted string is written to dest.
 * @param n The function write at most n bytes (including the trailing '\0')
 *          to dest.
 * @param size a file size in bytes.
 * @returns the number of characters printed (not including the trailing '\0').
 *          If the output is truncated, the number of characters that would
 *          have been written to the string will be returned. A negative value
 *          is returned if an output error is encountered.
 */
int kibi_n_format_size_detailed(char *dest, size_t n, kibi_filesize_t size);

/**
 * Formats a transfer rate into a human readable string.
 * Sizes are rounded to the nearest size prefix with at least three
 * significant digits. The prefix units base is 1000 or 1024 depending on
 * the user's configuration.
 *
 * @param dest The formatted string is written to dest.
 * @param n The function write at most n bytes (including the trailing '\0')
 *          to dest.
 * @param transfer_rate a transfer rate in bytes/s.
 * @returns the number of characters printed (not including the trailing '\0').
 *          If the output is truncated, the number of characters that would
 *          have been written to the string will be returned. A negative value
 *          is returned if an output error is encountered.
 */
int kibi_n_format_transfer_rate(char *dest, size_t n,
                                kibi_filesize_t transfer_rate);

/**
 * Formats a value into a human readable string.
 * Sizes are rounded to the nearest size prefix with at least three
 * significant digits. The prefix units base is 1000 or 1024 depending on
 * the user's configuration. The suffix is appended to the string and should
 * contain the unit.
 *
 * Use this function with care! kibi_n_format_size() should be used for file
 * sizes instead. kibi_n_format_transfer_rate() should be used for transfer
 * rates instead.
 *
 * @param dest The formatted string is written to dest.
 * @param n The function write at most n bytes (including the trailing '\0')
 *          to dest.
 * @param value a value that should be formatted.
 * @param suffix a string containing the unit.
 * @returns the number of characters printed (not including the trailing '\0').
 *          If the output is truncated, the number of characters that would
 *          have been written to the string will be returned. A negative value
 *          is returned if an output error is encountered.
 */
int kibi_n_format_value(char *dest, size_t n, kibi_filesize_t value,
                        const char *suffix);

#undef g_format_size_for_display
#define g_format_size_for_display kibi_format_size

#endif // __KIBI_H__
