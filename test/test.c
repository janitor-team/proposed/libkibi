/*
 * Copyright (c) 2010-2011, Benjamin Drung <benjamin.drung@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

// C standard libraries
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "kibi.h"

#define DEST_BUFFER_LENGTH 20
#define PREFIX_TYPES 3

typedef struct {
    kibi_filesize_t size;
    char *format[PREFIX_TYPES];
} testcase_t;

typedef struct {
    kibi_filesize_t size;
    const char *suffix;
    char *format[PREFIX_TYPES];
} testcase2_t;

typedef struct {
    kibi_filesize_t size;
    unsigned int level;
    double divided_size[PREFIX_TYPES];
} testcase_divide_t;

typedef struct {
    double size;
    unsigned int level;
    kibi_filesize_t multiplied_size[PREFIX_TYPES];
} testcase_multiply_t;

typedef struct {
    kibi_filesize_t size;
    size_t length;
    char *format[PREFIX_TYPES];
    int expected_length[PREFIX_TYPES];
} testcase_n_t;

typedef struct {
    kibi_filesize_t size;
    const char *suffix;
    size_t length;
    char *format[PREFIX_TYPES];
    int expected_length[PREFIX_TYPES];
} testcase2_n_t;

typedef struct {
    char *name;
    char *(*function)(kibi_filesize_t);
    const testcase_t *testcases;
} function_test_t;

typedef struct {
    char *name;
    char *(*function)(kibi_filesize_t, const char*);
    const testcase2_t *testcases;
} function2_test_t;

typedef struct {
    char *name;
    const char *(*function)(unsigned int);
    const testcase_t *testcases;
} const_function_test_t;

typedef struct {
    char *name;
    int (*function)(char*, size_t, kibi_filesize_t);
    const testcase_t *testcases;
    const testcase_n_t *testcases_n;
} function_n_test_t;

typedef struct {
    char *name;
    int (*function)(char*, size_t, kibi_filesize_t, const char*);
    const testcase2_t *testcases;
    const testcase2_n_t *testcases_n;
} function2_n_test_t;

static const char *prefix[PREFIX_TYPES] = {"base10", "base2", "historic"};

static const testcase_divide_t testcases_divide_size[] = {
    {0, 4, {0, 0, 0}},
    {6564, 0, {6564, 6564, 6564}},
    {9873498, 1, {9873.498, 9642.087890625, 9642.087890625}},
    {3214984, 2, {3.214984, 3.066047668, 3.066047668}},
    {34985, 3, {3.4985e-5, 3.258232027e-5, 3.258232027e-5}},
    {7965891352, 4, {7.965891352e-3, 7.24493598e-3, 7.24493598e-3}},
    {68948761, 5, {6.8948761e-8, 6.123880159e-8, 6.123880159e-8}},
    {98761673753213, 6, {9.8761673753213e-5, 8.566209699e-5, 8.566209699e-5}},
    {98761673753213, 7, {9.8761673753213e-5, 8.566209699e-5, 8.566209699e-5}},
    {23456765, 42, {2.3456765e-11, 2.034550046e-11, 2.034550046e-11}},
    {0, 0, {-2, -2, -2}},
};

static const testcase_t testcases_format_size[] = {
    {0, {"0 B", "0 B", "0 B"}},
    {999, {"999 B", "999 B", "999 B"}},
    {1000, {"1.00 kB", "1000 B", "1000 B"}},
    {1023, {"1.02 kB", "1023 B", "1023 B"}},
    {1024, {"1.02 kB", "1.00 KiB", "1.00 KB"}},
    {4096, {"4.10 kB", "4.00 KiB", "4.00 KB"}},
    {9994, {"9.99 kB", "9.76 KiB", "9.76 KB"}},
    {9995, {"10.0 kB", "9.76 KiB", "9.76 KB"}},
    {10234, {"10.2 kB", "9.99 KiB", "9.99 KB"}},
    {10235, {"10.2 kB", "10.0 KiB", "10.0 KB"}},
    {999949, {"999.9 kB", "976.5 KiB", "976.5 KB"}},
    {999950, {"1.00 MB", "976.5 KiB", "976.5 KB"}},
    {1023948, {"1.02 MB", "999.9 KiB", "999.9 KB"}},
    {1023949, {"1.02 MB", "1000 KiB", "1000 KB"}},
    {1048063, {"1.05 MB", "1023 KiB", "1023 KB"}},
    {1048064, {"1.05 MB", "1.00 MiB", "1.00 MB"}},
    {734003200, {"734.0 MB", "700.0 MiB", "700.0 MB"}},
    {1073167834, {"1.07 GB", "1023 MiB", "1023 MB"}},
    {1073217535, {"1.07 GB", "1023 MiB", "1023 MB"}},
    {1073217536, {"1.07 GB", "1.00 GiB", "1.00 GB"}},
    {4700000000, {"4.70 GB", "4.38 GiB", "4.38 GB"}},
    {8500000000, {"8.50 GB", "7.92 GiB", "7.92 GB"}},
    {1073688136908, {"1.07 TB", "999.9 GiB", "999.9 GB"}},
    {1073688136909, {"1.07 TB", "1000 GiB", "1000 GB"}},
    {1098974756863, {"1.10 TB", "1023 GiB", "1023 GB"}},
    {1098974756864, {"1.10 TB", "1.00 TiB", "1.00 TB"}},
    {3044999999909, {"3.04 TB", "2.77 TiB", "2.77 TB"}},
    {3045000000000, {"3.05 TB", "2.77 TiB", "2.77 TB"}},
    {46234463947980, {"46.2 TB", "42.0 TiB", "42.0 TB"}},
    {46234463947981, {"46.2 TB", "42.1 TiB", "42.1 TB"}},
    {999949999999999, {"999.9 TB", "909.4 TiB", "909.4 TB"}},
    {999950000000000, {"1.00 PB", "909.4 TiB", "909.4 TB"}},
    {1125350151028735, {"1.13 PB", "1023 TiB", "1023 TB"}},
    {1125350151028736, {"1.13 PB", "1.00 PiB", "1.00 PB"}},
    {999949999999999999, {"999.9 PB", "888.1 PiB", "888.1 PB"}},
    {999950000000000000, {"1.00 EB", "888.1 PiB", "888.1 PB"}},
    {1125843611847281868, {"1.13 EB", "999.9 PiB", "999.9 PB"}},
    {1125843611847281869, {"1.13 EB", "1000 PiB", "1000 PB"}},
    {1152358554653425663, {"1.15 EB", "1023 PiB", "1023 PB"}},
    {1152358554653425664, {"1.15 EB", "1.00 EiB", "1.00 EB"}},
    {6681180119196678225ULL, {"6.68 EB", "5.79 EiB", "5.79 EB"}},
    {6681180119196678226ULL, {"6.68 EB", "5.80 EiB", "5.80 EB"}},
    {9994999999999999999ULL, {"9.99 EB", "8.67 EiB", "8.67 EB"}},
    {9995000000000000000ULL, {"10.0 EB", "8.67 EiB", "8.67 EB"}},
    {11523450438545435525ULL, {"11.5 EB", "9.99 EiB", "9.99 EB"}},
    {11523450438545435526ULL, {"11.5 EB", "10.0 EiB", "10.0 EB"}},
    {ULLONG_MAX, {"18.4 EB", "16.0 EiB", "16.0 EB"}},
    {0, {NULL, NULL, NULL}},
};

static const testcase_t testcases_format_size_detailed[] = {
    {42, {"42 B", "42 B", "42 B"}},
    {999, {"999 B", "999 B", "999 B"}},
    {1000, {"1.00 kB", "1000 B", "1000 B"}},
    {1023, {"1.02 kB", "1023 B", "1023 B"}},
    {1024, {"1.02 kB/1.00 KiB", "1.00 KiB/1.02 kB", "1.00 KB"}},
    {9435092375, {"9.44 GB/8.79 GiB", "8.79 GiB/9.44 GB", "8.79 GB"}},
    {250000000000, {"250.0 GB/232.8 GiB", "232.8 GiB/250.0 GB", "232.8 GB"}},
    {1086241659657, {"1.09 TB/1012 GiB", "1012 GiB/1.09 TB", "1012 GB"}},
    {2000000000000, {"2.00 TB/1.82 TiB", "1.82 TiB/2.00 TB", "1.82 TB"}},
    {0, {NULL, NULL, NULL}},
};

static const testcase_t testcases_format_memory_size[] = {
    {655360, {"640.0 KiB", "640.0 KiB", "640.0 KB"}},
    {536870912, {"512.0 MiB", "512.0 MiB", "512.0 MB"}},
    {805306368, {"768.0 MiB", "768.0 MiB", "768.0 MB"}},
    {4294967296, {"4.00 GiB", "4.00 GiB", "4.00 GB"}},
    {9063598475374, {"8.24 TiB", "8.24 TiB", "8.24 TB"}},
    {0, {NULL, NULL, NULL}},
};

static const testcase_t testcases_format_transfer_rate[] = {
    {42, {"42 B/s", "42 B/s", "42 B/s"}},
    {12390, {"12.4 kB/s", "12.1 KiB/s", "12.1 KB/s"}},
    {4000000, {"4.00 MB/s", "3.81 MiB/s", "3.81 MB/s"}},
    {12500000, {"12.5 MB/s", "11.9 MiB/s", "11.9 MB/s"}},
    {125000000, {"125.0 MB/s", "119.2 MiB/s", "119.2 MB/s"}},
    {0, {NULL, NULL, NULL}},
};

static const testcase2_t testcases_format_si_unit[] = {
    {854, "%p%s\"%x", {"854 %p%s\"%x", "854 %p%s\"%x", "854 %p%s\"%x"}},
    {68803, NULL, {"68.8 k(null)", "68.8 k(null)", "68.8 k(null)"}},
    {3166666667, "", {"3.17 G", "3.17 G", "3.17 G"}},
    {3166666667, "Hz", {"3.17 GHz", "3.17 GHz", "3.17 GHz"}},
    {0, NULL, {NULL, NULL, NULL}},
};

static const testcase2_t testcases_format_value[] = {
    {937, "bar%s\"%x", {"937 bar%s\"%x", "937 bar%s\"%x", "937 bar%s\"%x"}},
    {3830854, NULL, {"3.83 M(null)", "3.65 Mi(null)", "3.65 M(null)"}},
    {3830854, "", {"3.83 M", "3.65 Mi", "3.65 M"}},
    {3830854, "B", {"3.83 MB", "3.65 MiB", "3.65 MB"}},
    {3830854, "egafoo", {"3.83 Megafoo", "3.65 Miegafoo", "3.65 Megafoo"}},
    {3830854, "bit/h", {"3.83 Mbit/h", "3.65 Mibit/h", "3.65 Mbit/h"}},
    {0, NULL, {NULL, NULL, NULL}},
};

static const testcase_t testcases_get_unit_for_size[] = {
    {0, {"B", "B", "B"}},
    {1, {"kB", "KiB", "KB"}},
    {2, {"MB", "MiB", "MB"}},
    {3, {"GB", "GiB", "GB"}},
    {4, {"TB", "TiB", "TB"}},
    {5, {"PB", "PiB", "PB"}},
    {6, {"EB", "EiB", "EB"}},
    {7, {"EB", "EiB", "EB"}},
    {42, {"EB", "EiB", "EB"}},
    {0, {NULL, NULL, NULL}},
};

static const testcase_t testcases_get_unit_for_transfer_rate[] = {
    {0, {"B/s", "B/s", "B/s"}},
    {1, {"kB/s", "KiB/s", "KB/s"}},
    {2, {"MB/s", "MiB/s", "MB/s"}},
    {3, {"GB/s", "GiB/s", "GB/s"}},
    {4, {"TB/s", "TiB/s", "TB/s"}},
    {5, {"PB/s", "PiB/s", "PB/s"}},
    {6, {"EB/s", "EiB/s", "EB/s"}},
    {7, {"EB/s", "EiB/s", "EB/s"}},
    {42, {"EB/s", "EiB/s", "EB/s"}},
    {0, {NULL, NULL, NULL}},
};

static const testcase_multiply_t testcases_multiply_size[] = {
    {6548.49, 0, {6548, 6548, 6548}},
    {6548.51, 0, {6549, 6549, 6549}},
    {2.847868, 1, {2848, 2916, 2916}},
    {983276.3987432, 2, {983276398743, 1031040033089, 1031040033089}},
    {5.67, 3, {5670000000, 6088116142, 6088116142}},
    {54.89, 4, {54890000000000, 60352193248625, 60352193248625}},
    {3.158971567e-11, 5, {31590, 35567, 35567}},
    {8.346598654e-10, 6, {834659865, 962297308, 962297308}},
    {8.346598654e-10, 7, {834659865, 962297308, 962297308}},
    {42e-18, 42, {42, 48, 48}},
    {-2000, 0, {0, 0, 0}},
};

static const testcase_n_t testcases_n_format_memory_size[] = {
    {342987, 1, {"", "", ""}, {9, 9, 8}},
    {342987, 3, {"33", "33", "33"}, {9, 9, 8}},
    {342987, 8, {"334.9 K", "334.9 K", "334.9 K"}, {9, 9, 8}},
    {342987, 9, {"334.9 Ki", "334.9 Ki", "334.9 KB"}, {9, 9, 8}},
    {342987, 10, {"334.9 KiB", "334.9 KiB", "334.9 KB"}, {9, 9, 8}},
    {2983290, 0, {"", "", ""}, {8, 8, 7}},
    {1055916032, 5, {"1007", "1007", "1007"}, {8, 8, 7}},
    {0, 0, {NULL, NULL, NULL}, {0, 0, 0}},
};

static const testcase2_n_t testcases_n_format_si_unit[] = {
    {180000000, "Hz", 1, {"", "", ""}, {9, 9, 9}},
    {180000000, "Hz", 5, {"180.", "180.", "180."}, {9, 9, 9}},
    {180000000, "Hz", 6, {"180.0", "180.0", "180.0"}, {9, 9, 9}},
    {180000000, "Hz", 7, {"180.0 ", "180.0 ", "180.0 "}, {9, 9, 9}},
    {180000000, "Hz", 8, {"180.0 M", "180.0 M", "180.0 M"}, {9, 9, 9}},
    {180000000, "Hz", 9, {"180.0 MH", "180.0 MH", "180.0 MH"}, {9, 9, 9}},
    {180000000, "Hz", 10, {"180.0 MHz", "180.0 MHz", "180.0 MHz"}, {9, 9, 9}},
    {0, 0, 0, {NULL, NULL, NULL}, {0, 0, 0}},
};

static const testcase_n_t testcases_n_format_size[] = {
    {124734246, 0, {"", "", ""}, {8, 9, 8}},
    {124734246, 5, {"124.", "119.", "119."}, {8, 9, 8}},
    {124734246, 8, {"124.7 M", "119.0 M", "119.0 M"}, {8, 9, 8}},
    {124734246, 9, {"124.7 MB", "119.0 Mi", "119.0 MB"}, {8, 9, 8}},
    {124734246, 10, {"124.7 MB", "119.0 MiB", "119.0 MB"}, {8, 9, 8}},
    {0, 0, {NULL, NULL, NULL}, {0, 0, 0}},
};

static const testcase_n_t testcases_n_format_size_detailed[] = {
    {22398642, 0, {"", "", ""}, {16, 16, 7}},
    {22398642, 4, {"22.", "21.", "21."}, {16, 16, 7}},
    {22398642, 8, {"22.4 MB", "21.4 Mi", "21.4 MB"}, {16, 16, 7}},
    {22398642, 9, {"22.4 MB/", "21.4 MiB", "21.4 MB"}, {16, 16, 7}},
    {22398642, 10, {"22.4 MB/2", "21.4 MiB/", "21.4 MB"}, {16, 16, 7}},
    {22398642, 11, {"22.4 MB/21", "21.4 MiB/2", "21.4 MB"}, {16, 16, 7}},
    {22398642, 12, {"22.4 MB/21.", "21.4 MiB/22", "21.4 MB"}, {16, 16, 7}},
    {22398642, 16, {"22.4 MB/21.4 Mi", "21.4 MiB/22.4 M", "21.4 MB"},
     {16, 16, 7}},
    {22398642, 17, {"22.4 MB/21.4 MiB", "21.4 MiB/22.4 MB", "21.4 MB"},
     {16, 16, 7}},
    {0, 0, {NULL, NULL, NULL}, {0, 0, 0}},
};

static const testcase_n_t testcases_n_format_transfer_rate[] = {
    {9847354654, 0, {"", "", ""}, {9, 10, 9}},
    {9847354654, 6, {"9.85 ", "9.17 ", "9.17 "}, {9, 10, 9}},
    {9847354654, 8, {"9.85 GB", "9.17 Gi", "9.17 GB"}, {9, 10, 9}},
    {9847354654, 9, {"9.85 GB/", "9.17 GiB", "9.17 GB/"}, {9, 10, 9}},
    {9847354654, 10, {"9.85 GB/s", "9.17 GiB/", "9.17 GB/s"}, {9, 10, 9}},
    {9847354654, 11, {"9.85 GB/s", "9.17 GiB/s", "9.17 GB/s"}, {9, 10, 9}},
    {0, 0, {NULL, NULL, NULL}, {0, 0, 0}},
};

static const testcase2_n_t testcases_n_format_value[] = {
    {9834354647232, "euro", 0, {"", "", ""}, {10, 11, 10}},
    {9834354647232, "euro", 4, {"9.8", "8.9", "8.9"}, {10, 11, 10}},
    {9834354647232, "euro", 5, {"9.83", "8.94", "8.94"}, {10, 11, 10}},
    {9834354647232, "euro", 6, {"9.83 ", "8.94 ", "8.94 "}, {10, 11, 10}},
    {9834354647232, "euro", 7, {"9.83 T", "8.94 T", "8.94 T"}, {10, 11, 10}},
    {9834354647232, "euro", 8, {"9.83 Te", "8.94 Ti", "8.94 Te"}, {10, 11, 10}},
    {9834354647232, "euro", 10, {"9.83 Teur", "8.94 Tieu", "8.94 Teur"},
     {10, 11, 10}},
    {9834354647232, "euro", 11, {"9.83 Teuro", "8.94 Tieur", "8.94 Teuro"},
     {10, 11, 10}},
    {9834354647232, "euro", 12, {"9.83 Teuro", "8.94 Tieuro", "8.94 Teuro"},
     {10, 11, 10}},
    {0, 0, 0, {NULL, NULL, NULL}, {0, 0, 0}},
};

static const function_test_t function_tests[] = {
    {"kibi_format_memory_size", kibi_format_memory_size,
     testcases_format_memory_size},
    {"kibi_format_size", kibi_format_size, testcases_format_size},
    {"kibi_format_size_detailed", kibi_format_size_detailed,
     testcases_format_size_detailed},
    {"kibi_format_transfer_rate", kibi_format_transfer_rate,
     testcases_format_transfer_rate},
    {NULL, NULL, NULL},
};

static const function2_test_t function2_tests[] = {
    {"kibi_format_si_unit", kibi_format_si_unit, testcases_format_si_unit},
    {"kibi_format_value", kibi_format_value, testcases_format_value},
    {NULL, NULL, NULL},
};

static const const_function_test_t const_function_tests[] = {
    {"kibi_get_unit_for_size", kibi_get_unit_for_size,
     testcases_get_unit_for_size},
    {"kibi_get_unit_for_transfer_rate", kibi_get_unit_for_transfer_rate,
     testcases_get_unit_for_transfer_rate},
    {NULL, NULL, NULL},
};

static const function_n_test_t function_n_tests[] = {
    {"kibi_n_format_memory_size", kibi_n_format_memory_size,
     testcases_format_memory_size, testcases_n_format_memory_size},
    {"kibi_n_format_size", kibi_n_format_size, testcases_format_size,
     testcases_n_format_size},
    {"kibi_n_format_size_detailed", kibi_n_format_size_detailed,
     testcases_format_size_detailed, testcases_n_format_size_detailed},
    {"kibi_n_format_transfer_rate", kibi_n_format_transfer_rate,
     testcases_format_transfer_rate, testcases_n_format_transfer_rate},
    {NULL, NULL, NULL, NULL},
};

static const function2_n_test_t function2_n_tests[] = {
    {"kibi_n_format_si_unit", kibi_n_format_si_unit, testcases_format_si_unit,
     testcases_n_format_si_unit},
    {"kibi_n_format_value", kibi_n_format_value, testcases_format_value,
     testcases_n_format_value},
    {NULL, NULL, NULL, NULL},
};

static bool equal(double a, double b) {
    double diff = a - b;
    return (-1e-9 < diff) && (diff < 1e-9);
}

static int check_divide(char *name,
                        double (*function)(kibi_filesize_t, unsigned int),
                        const testcase_divide_t *testcases, int format) {
    double result;
    int failures = 0;
    const testcase_divide_t *testcase;

    for(testcase = testcases; testcase->divided_size[0] > -1; testcase++) {
        printf("check %s(%llu, %u)... ", name, testcase->size, testcase->level);
        fflush(stdout);
        result = function(testcase->size, testcase->level);
        if(equal(result, testcase->divided_size[format])) {
            failures += 0;
            printf("\"%g\" (OK)\n", result);
        } else {
            failures += 1;
            printf("%g != %g (FAILED)\n", result,
                   testcase->divided_size[format]);
            printf("%g\n", result - testcase->divided_size[format]);
        }
    }

    return failures;
}

static int check_function(char *name, char *(*function)(kibi_filesize_t),
                          const testcase_t *testcases, int format) {
    char *result;
    int failures = 0;
    const testcase_t *testcase;

    for(testcase = testcases; testcase->format[0] != NULL; testcase++) {
        printf("check %s(%llu)... ", name, testcase->size);
        fflush(stdout);
        result = function(testcase->size);
        if(strcmp(testcase->format[format], result) == 0) {
            failures += 0;
            printf("\"%s\" (OK)\n", result);
        } else {
            failures += 1;
            printf("\"%s\" != \"%s\" (FAILED)\n", result,
                   testcase->format[format]);
        }
        free(result);
    }

    return failures;
}

static int check_function2(char *name,
                           char *(*function)(kibi_filesize_t, const char*),
                           const testcase2_t *testcases, int format) {
    char *result;
    int failures = 0;
    const testcase2_t *testcase;

    for(testcase = testcases; testcase->format[0] != NULL; testcase++) {
        printf("check %s(%llu, \"%s\")... ", name, testcase->size,
               testcase->suffix);
        fflush(stdout);
        result = function(testcase->size, testcase->suffix);
        if(strcmp(testcase->format[format], result) == 0) {
            failures += 0;
            printf("\"%s\" (OK)\n", result);
        } else {
            failures += 1;
            printf("\"%s\" != \"%s\" (FAILED)\n", result,
                   testcase->format[format]);
        }
        free(result);
    }

    return failures;
}

static int check_const_function(char *name,
                                const char *(*function)(unsigned int),
                                const testcase_t *testcases, int format) {
    const char *result;
    unsigned int level;
    int failures = 0;
    const testcase_t *testcase;

    for(testcase = testcases; testcase->format[0] != NULL; testcase++) {
        level = (unsigned int)testcase->size;
        printf("check %s(%u)... ", name, level);
        fflush(stdout);
        result = function(level);
        if(strcmp(testcase->format[format], result) == 0) {
            failures += 0;
            printf("\"%s\" (OK)\n", result);
        } else {
            failures += 1;
            printf("\"%s\" != \"%s\" (FAILED)\n", result,
                   testcase->format[format]);
        }
    }

    return failures;
}

static int check_function_n(char *name,
                            int (*function)(char*, size_t, kibi_filesize_t),
                            const testcase_t *testcases,
                            const testcase_n_t *testcases_n, int format) {
    bool strings_equal;
    char fix_formatted_size[DEST_BUFFER_LENGTH];
    char *formatted_size;
    int length;
    int failures = 0;
    const testcase_t *testcase;
    const testcase_n_t *testcase_n;

    for(testcase = testcases; testcase->format[0] != NULL; testcase++) {
        printf("check %s(dest, %i, %llu)... ", name, DEST_BUFFER_LENGTH,
               testcase->size);
        fflush(stdout);
        length = function(fix_formatted_size, DEST_BUFFER_LENGTH,
                          testcase->size);
        strings_equal = strncmp(testcase->format[format], fix_formatted_size,
                                DEST_BUFFER_LENGTH) == 0;
        if(strings_equal && (length == (int)strlen(testcase->format[format]))) {
            failures += 0;
            printf("dest=\"%s\" (OK)\n", fix_formatted_size);
        } else if(!strings_equal) {
            failures += 1;
            printf("\"%s\" != \"%s\" (FAILED)\n", fix_formatted_size,
                   testcase->format[format]);
        } else {
            failures += 1;
            printf("%i != %zu (FAILED)\n", length,
                   strlen(testcase->format[format]));
        }
    }

    for(testcase_n = testcases_n; testcase_n->format[0] != NULL; testcase_n++) {
        printf("check %s(dest, %zu, %llu)... ", name, testcase_n->length,
               testcase_n->size);
        fflush(stdout);
        if(testcase_n->length == 0) {
            formatted_size = NULL;
        } else {
            formatted_size = malloc(testcase_n->length);
            if(formatted_size == NULL) {
                printf("malloc(%zu) FAILED\n", testcase_n->length);
                continue;
            }
        }
        length = function(formatted_size, testcase_n->length, testcase_n->size);
        if(testcase_n->length == 0) {
            strings_equal = formatted_size == NULL;
        } else {
            strings_equal = strncmp(testcase_n->format[format], formatted_size,
                                    testcase_n->length) == 0;
        }
        if(strings_equal && (length == testcase_n->expected_length[format])) {
            failures += 0;
            printf("dest=\"%s\" (OK)\n", formatted_size);
        } else if(!strings_equal) {
            failures += 1;
            printf("\"%s\" != \"%s\" (FAILED)\n", formatted_size,
                   testcase_n->format[format]);
        } else {
            failures += 1;
            printf("%i != %i (FAILED)\n", length,
                   testcase_n->expected_length[format]);
        }
        free(formatted_size);
    }

    return failures;
}

static int check_function2_n(char *name,
                             int (*function)(char*, size_t, kibi_filesize_t,
                                             const char*),
                             const testcase2_t *testcases,
                             const testcase2_n_t *testcases_n, int format) {
    bool strings_equal;
    char fix_formatted_size[DEST_BUFFER_LENGTH];
    char *formatted_size;
    int length;
    int failures = 0;
    const testcase2_t *testcase;
    const testcase2_n_t *testcase_n;

    for(testcase = testcases; testcase->format[0] != NULL; testcase++) {
        printf("check %s(dest, %i, %llu, \"%s\")... ", name, DEST_BUFFER_LENGTH,
               testcase->size, testcase->suffix);
        fflush(stdout);
        length = function(fix_formatted_size, DEST_BUFFER_LENGTH,
                          testcase->size, testcase->suffix);
        strings_equal = strncmp(testcase->format[format], fix_formatted_size,
                                DEST_BUFFER_LENGTH) == 0;
        if(strings_equal && (length == (int)strlen(testcase->format[format]))) {
            failures += 0;
            printf("dest=\"%s\" (OK)\n", fix_formatted_size);
        } else if(!strings_equal) {
            failures += 1;
            printf("\"%s\" != \"%s\" (FAILED)\n", fix_formatted_size,
                   testcase->format[format]);
        } else {
            failures += 1;
            printf("%i != %zu (FAILED)\n", length,
                   strlen(testcase->format[format]));
        }
    }

    for(testcase_n = testcases_n; testcase_n->format[0] != NULL; testcase_n++) {
        printf("check %s(dest, %zu, %llu, \"%s\")... ", name,
               testcase_n->length, testcase_n->size, testcase_n->suffix);
        fflush(stdout);
        if(testcase_n->length == 0) {
            formatted_size = NULL;
        } else {
            formatted_size = malloc(testcase_n->length);
            if(formatted_size == NULL) {
                printf("malloc(%zu) FAILED\n", testcase_n->length);
                continue;
            }
        }
        length = function(formatted_size, testcase_n->length, testcase_n->size,
                          testcase_n->suffix);
        if(testcase_n->length == 0) {
            strings_equal = formatted_size == NULL;
        } else {
            strings_equal = strncmp(testcase_n->format[format], formatted_size,
                                    testcase_n->length) == 0;
        }
        if(strings_equal && (length == testcase_n->expected_length[format])) {
            failures += 0;
            printf("dest=\"%s\" (OK)\n", formatted_size);
        } else if(!strings_equal) {
            failures += 1;
            printf("\"%s\" != \"%s\" (FAILED)\n", formatted_size,
                   testcase_n->format[format]);
        } else {
            failures += 1;
            printf("%i != %i (FAILED)\n", length,
                   testcase_n->expected_length[format]);
        }
        free(formatted_size);
    }

    return failures;
}

static int check_multiply(char *name,
                          kibi_filesize_t (*function)(double, unsigned int),
                          const testcase_multiply_t *testcases, int format) {
    kibi_filesize_t result;
    int failures = 0;
    const testcase_multiply_t *testcase;

    for(testcase = testcases; testcase->size > -1000; testcase++) {
        printf("check %s(%g, %u)... ", name, testcase->size, testcase->level);
        fflush(stdout);
        result = function(testcase->size, testcase->level);
        if(result == testcase->multiplied_size[format]) {
            failures += 0;
            printf("%llu (OK)\n", result);
        } else {
            failures += 1;
            printf("%llu != %llu (FAILED)\n", result,
                   testcase->multiplied_size[format]);
        }
    }

    return failures;
}

static int get_format(const char *value) {
    int i;
    int format = -1;

    for(i = 0; i < PREFIX_TYPES; i++) {
        if(strcmp(prefix[i], value) == 0) {
            format = i;
            break;
        }
    }

    return format;
}

int main(int argc, char *argv[]) {
    int failures = 0;
    int format;
    const const_function_test_t *const_function_test;
    const function_n_test_t *function_n_test;
    const function_test_t *function_test;
    const function2_test_t *function2_test;
    const function2_n_test_t *function2_n_test;

    if(argc != 2) {
        printf("Usage: %s <format>\n", argv[0]);
        return 1;
    }

    format = get_format(argv[1]);
    if(format < 0 || format >= PREFIX_TYPES) {
        printf("%s: Error: Failed to determine format \"%s\".\n", argv[0],
               argv[1]);
        return 1;
    }

    failures += check_divide("kibi_divide_size", kibi_divide_size,
                             testcases_divide_size, format);

    for(function_test = function_tests; function_test->name != NULL;
        function_test++) {
        failures += check_function(function_test->name, function_test->function,
                                   function_test->testcases, format);
    }

    for(function2_test = function2_tests; function2_test->name != NULL;
        function2_test++) {
        failures += check_function2(function2_test->name,
                                    function2_test->function,
                                    function2_test->testcases, format);
    }

    for(const_function_test = const_function_tests;
        const_function_test->name != NULL; const_function_test++) {
        failures += check_const_function(const_function_test->name,
                                         const_function_test->function,
                                         const_function_test->testcases,
                                         format);
    }

    failures += check_multiply("kibi_multiply_size", kibi_multiply_size,
                               testcases_multiply_size, format);

    for(function_n_test = function_n_tests; function_n_test->name != NULL;
        function_n_test++) {
        failures += check_function_n(function_n_test->name,
                                     function_n_test->function,
                                     function_n_test->testcases,
                                     function_n_test->testcases_n, format);
    }

    for(function2_n_test = function2_n_tests; function2_n_test->name != NULL;
        function2_n_test++) {
        failures += check_function2_n(function2_n_test->name,
                                      function2_n_test->function,
                                      function2_n_test->testcases,
                                      function2_n_test->testcases_n, format);
    }

    return failures;
}
